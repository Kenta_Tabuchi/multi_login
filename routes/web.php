<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::resource('/',\App\Http\Controllers\Web\TopController::class);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function(){
    Route::get('login', [App\Http\Controllers\Admin\Auth\LoginController::class,'showLoginForm']);
    Route::post('login', [App\Http\Controllers\Admin\Auth\LoginController::class,'login'])->name('login');
    Route::post('logout', [App\Http\Controllers\Admin\Auth\LoginController::class,'logout'])->name('logout');
    // 管理者はログイン画面から登録させないためコメントアウトしている。
    // Route::get('register', [App\Http\Controllers\Admin\Auth\RegistesrController::class,'showRegistrationForm'])->name('register');
    // Route::post('register', [App\Http\Controllers\Admin\Auth\RegisterController::class,'register']);

    Route::get('/home', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('home');

});
